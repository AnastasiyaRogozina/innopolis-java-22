import table.Table;


public class Main {
    public static void main(String[] args) {
        Table table1 = new Table();
        table1.add("Марсель", 28);
        table1.add("Максим", 24);

        Table table2 = new Table();
        table2.add("Виктор", 29);
        table2.add("Алексей", 24);
        table2.add("Виктор", 32);

        Table.Printer printer1;
        Table.Printer printer2;

        printer1 = table1.new Printer("^");
        printer2 = table2.new Printer("+");

        printer1.print();
        printer2.print();


        // обращение к вложенному классу
//        Table.Pair pair = new Table.Pair("Вася", 29);


    }
}
