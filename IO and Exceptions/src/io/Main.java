package io;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class Main {
    public static void main(String[] args) {
        // возможно, файл не существует, а мы пытаемся его открыть

        try {
            Reader reader = new FileReader("input.txt");
            char[] characters = new char[100];
            characters[0] = (char)reader.read();
            characters[1] = (char)reader.read();
            characters[2] = (char)reader.read();
            characters[3] = (char)reader.read();
            characters[4] = (char)reader.read();
            System.out.println(reader.read());
            int i = 0;
        } catch (FileNotFoundException e) {
            System.out.println("Возникла ошибка " + e.getMessage());
        } catch (IOException e) {
            System.out.println("Возникла ошибка в процессе чтения");
        }
    }
}