package lambda;

// класс, который преобразует массив чисел в другой массив по правилу, которое
// описано в NumbersProcessor
public class ArrayNumbersConverter {
    public int[] convert(int[] array, NumbersProcessor processor) {
        // создаем массив, такого же размера, как и исходный
        int[] newArray = new int[array.length];
        // пробегаю все элементы исходного массива
        for (int i = 0; i < array.length; i++) {
            // преобразую каждое число исходного массива
            // над каждым числом вызываем метод process интерфейса NumberProcessor
            int newNumber = processor.process(array[i]);
            // кладем новое число в новый массив
            newArray[i] = newNumber;
        }
        // результат работы метода - новый массив
        return newArray;
    }
}
