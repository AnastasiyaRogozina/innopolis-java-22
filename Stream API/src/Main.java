import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        // создали список слов
        List<String> words = List.of("Hello", "Bye", "how", "are", "You");
        // реализовали анонимный класс на основе функционального интерфейса
        Predicate<String> isUpperCase = new Predicate<String>() {
            @Override
            public boolean test(String word) {
                // проверяем, не является ли первый символ слова
                // большой буквой, если является - возвращаем true
                return Character.isUpperCase(word.toCharArray()[0]);
            }
        };

        // реализовали анонимный класс на основе функционального интерфейса
        Consumer<String> printWord = new Consumer<String>() {
            @Override
            public void accept(String word) {
                // просто выводим в консоль входное слово
                System.err.println(word);
            }
        };

        Function<String, String> allToUpperCase = new Function<String, String>() {
            @Override
            public String apply(String word) {
                return word.toUpperCase();
            }
        };

        // получили stream на основе списка слов
        Stream<String> wordsStream = words.stream();
        // применяем метод filter, которому скормили описанный ранее предикат,
        // и получили стрим, содержащий "отфильтрованные объекты"
        Stream<String> upperCaseWordsStream = wordsStream.filter(isUpperCase);

        Stream<String> allUpperCaseStream = upperCaseWordsStream.map(allToUpperCase);
        // применяем метод forEach над фильтрованным стримом и выводим все в консоль
        allUpperCaseStream.forEach(printWord);


    }
}