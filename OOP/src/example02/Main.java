package example02;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

// Создать массив из 1000 людей
// каждый человек имеет определенный возраст от 18 до 90
// нужно понять, люди какого возраста встречаются чаще других
// решить нужно за O(n)

// 0 - присутствие отсутствие
// null - отсутствие присутствия
public class Main {
    public static void main(String[] args) {
        int min = User.MIN_ADULT_AGE;
        int max = User.MAX_ADULT_AGE + 1;
        // массив людей, но он пока пустой (1000 элементов - null)
        User[] users = new User[1000];
        // создаю генератор случайных чисел
        Random random = new Random();
        // пробегаюсь по всем элементам массива и создаю человека для каждого элемента
        for (int i = 0; i < users.length; i++) {
            // имя каждого человека - User0, User1 и т/д
            String name = "User" + i;
            int age = random.nextInt(max - min) + min;
            // создаю на i-ой позиции в массиве нового пользователя
            users[i] = new User(name, age);
        }

//        users[14].age = 150;

        // 0 - 73
        // 18 - 90
        // 18 - 18 = 0
        // 90 - 18 = 72
        int[] counts = new int[73];

        for (int i = 0; i < users.length; i++) {
//            // получили текущего пользователя
//            User currentUser = users[i];
//            // получить его возраст
//            int currentAge = currentUser.getAge();
//            counts[currentAge]++;

            counts[users[i].getAge() - min]++;
        }

        for (int i = 0; i < counts.length; i++) {
            System.out.println((i + 18) + " - " + counts[i]);
        }
    }
}
