package example05;

public interface Professional {
    void giveMoney(int sum);
    void goVacation();
}
