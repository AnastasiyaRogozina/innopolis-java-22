package example05;

public class FrontendDeveloper extends Programmer {

    public FrontendDeveloper() {
        super("hello", "bye", "CSS/HTML/JS");
    }

    @Override
    public void go() {
        System.out.println("Я никуда не иду, я просто сижу и верстаю страницы");
    }

    @Override
    public void goVacation() {
        System.out.println("Я и так всю жизнь в отпуске :)");
    }
}
