package example02;

public class FrontendDeveloper extends Programmer {

    public FrontendDeveloper() {
        super("hello", "bye", "CSS/HTML/JS");
    }
}
