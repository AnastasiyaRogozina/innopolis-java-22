package example04;

public class Worker extends Human {
    public Worker(String firstName, String lastName) {
        super(firstName, lastName);
    }

    @Override
    public void go() {
        System.out.println("Я хожу только на работу");
    }

    @Override
    public void tellAbout() {
        System.out.println("Я строитель!");
    }

    public void build() {
        System.out.println("Строю дом");
    }


}
