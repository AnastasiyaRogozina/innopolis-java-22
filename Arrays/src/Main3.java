import java.util.Arrays;

public class Main3 {
    public static void main(String[] args) {
        int min, indexOfMin, temp;

        int[] array = {10, 13, 11, 12, 2, 4};

        for (int i = 0; i < array.length; i++) {
            min = array[i];
            indexOfMin = i;

            for (int j = i; j < array.length; j++) {
                if (array[j] < min) {
                    min = array[j];
                    indexOfMin = j;
                }
            }

            temp = array[i];
            array[i] = array[indexOfMin];
            array[indexOfMin] = temp;

        }

        System.out.println(Arrays.toString(array));
    }
}
