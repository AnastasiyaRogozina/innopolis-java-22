import java.util.Random;

public class SomeClass {
    static final int MAX_SIZE = 10;
    int a;
    static int b;

    static {
        Random random = new Random();
        b = random.nextInt(100);
    }

    public void printA() {
        System.out.println(a);
    }

    public static int getPowOfB() {
        return b * b;
    }
}
