package example04;

public class Human extends Thread {
    private final CreditCard creditCard;
    private String name;

    public Human(CreditCard creditCard, String name) {
        this.creditCard = creditCard;
        this.name = name;
    }

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            synchronized (creditCard) {
                if (creditCard.getAmount() > 0) {
                    System.out.println(name + " идет покупать...");
                    if (creditCard.buy(10)) {
                        System.out.println(name + " купил!");
                    } else {
                        System.out.println(name + " говорит эээээ.....");
                    }
                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    throw new IllegalStateException(e);
                }
            }
        }
    }
}
